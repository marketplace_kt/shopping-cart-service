package com.marketplace.shoppingcartservice;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.marketplace.shoppingcartservice.dto.CartItemDto;
import com.marketplace.shoppingcartservice.entity.CartItem;
import com.marketplace.shoppingcartservice.entity.PersonalDiscount;
import com.marketplace.shoppingcartservice.entity.Client;
import com.marketplace.shoppingcartservice.repositories.CartItemRepository;
import com.marketplace.shoppingcartservice.repositories.PersonalDiscountRepository;
import com.marketplace.shoppingcartservice.repositories.ClientRepository;
import com.marketplace.shoppingcartservice.services.CartItemService;
import com.marketplace.shoppingcartservice.util.CartItemMapper;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.transaction.annotation.Transactional;

import java.util.UUID;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * Тест контроллеров с маппингом /cart-item/...
 */

@SpringBootTest
@AutoConfigureMockMvc
public class CartItemControllerTests {


    @Autowired
    private MockMvc mvc;
    @Autowired
    private CartItemRepository cartItemRepository;
    @Autowired
    private PersonalDiscountRepository personalDiscountRepository;
    @Autowired
    private ClientRepository clientRepository;
    @Autowired
    private CartItemService cartItemService;
    @Autowired
    private CartItemMapper cartItemMapper;

    private CartItem cartitem;
    private CartItem cartItem2;
    private PersonalDiscount personalDiscount;
    private PersonalDiscount personalDiscount2;
    private Client client;
    private Client client2;
    private CartItemDto cartItemDto;

    @BeforeEach
    public void setUp() {
        client = new Client(UUID.randomUUID(), "Pol");
        clientRepository.save(client);
        personalDiscount = new PersonalDiscount();
        personalDiscount.setClient(client);
        personalDiscount.setDiscountPercent(10);
        personalDiscountRepository.save(personalDiscount);

        client2 = new Client(UUID.randomUUID(), "Brad");
        clientRepository.save(client2);
        personalDiscount2 = new PersonalDiscount();
        personalDiscount2.setClient(client2);
        personalDiscount2.setDiscountPercent(0);
        personalDiscountRepository.save(personalDiscount2);


        cartitem = new CartItem();
        cartitem.setItemId(UUID.randomUUID());
        cartitem.setIsDiscounted(true);
        cartitem.setClient(client);
        cartitem.setPrice(78l);
        cartitem.setCount(43);
        cartitem.setDiscountPrice(23l);
        cartItemRepository.save(cartitem);

        cartItemDto = cartItemMapper.convertItemToDto(cartitem);

        cartItem2 = new CartItem();
        cartItem2.setItemId(UUID.randomUUID());
        cartItem2.setIsDiscounted(true);
        cartItem2.setClient(client2);
        cartItem2.setPrice(780l);
        cartItem2.setCount(3);
        cartItem2.setDiscountPrice(230l);
        cartItemRepository.save(cartItem2);

    }

    @AfterEach
    public void setDown() {
        cartItemRepository.delete(cartitem);
        cartItemRepository.delete(cartItem2);
        personalDiscountRepository.delete(personalDiscount);
        personalDiscountRepository.delete(personalDiscount2);
        clientRepository.delete(client);
        clientRepository.delete(client2);

    }

    @Test
    @Transactional
    void testControllerAddCartItem() throws Exception {

        mvc.perform(MockMvcRequestBuilders
                        .post("/cart-item/{id}", client.getId())
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(mapToJson(cartItemDto))
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

    protected String mapToJson(Object obj) throws JsonProcessingException {
        ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.writeValueAsString(obj);
    }

    @Test
    void showAllCartItems() throws Exception {
        mvc.perform(MockMvcRequestBuilders.get("/cart-item/{id}",client.getId())
                        .accept(MediaType.APPLICATION_JSON)
                        .content(mapToJson(cartItemDto)))
                .andExpect(status().isOk());
    }

}
