package com.marketplace.shoppingcartservice.service;

import com.marketplace.shoppingcartservice.exception.FailedToDeleteItemsException;
import com.marketplace.shoppingcartservice.exception.UserNotFoundException;
import com.marketplace.shoppingcartservice.ShoppingCartServiceApplication;
import com.marketplace.shoppingcartservice.entity.Profile;
import com.marketplace.shoppingcartservice.repositories.CartItemRepository;
import com.marketplace.shoppingcartservice.repositories.ProfileRepository;
import com.marketplace.shoppingcartservice.services.CartItemServices;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.DynamicPropertyRegistry;
import org.springframework.test.context.DynamicPropertySource;
import org.springframework.test.context.TestPropertySource;
import org.testcontainers.containers.PostgreSQLContainer;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;


/**
 * CartItemService Testcontainer
 */

@Testcontainers
@DirtiesContext
@SpringBootTest
@AutoConfigureMockMvc
@TestPropertySource(properties = {"spring.liquibase.enabled=true"})
@ContextConfiguration(classes = ShoppingCartServiceApplication.class)
public class CartItemServicesTest {

    @Container
    public static PostgreSQLContainer<?> postgreSQLContainer = new PostgreSQLContainer<>("postgres:13.2")
            .withDatabaseName("shopping_cart")
            .withPassword("admin")
            .withUsername("postgres");

    @DynamicPropertySource
    static void postgresqlProperties(DynamicPropertyRegistry registry) {
        registry.add("spring.datasource.url", postgreSQLContainer::getJdbcUrl);
        registry.add("spring.datasource.password", postgreSQLContainer::getPassword);
        registry.add("spring.datasource.username", postgreSQLContainer::getUsername);
    }

    @Autowired
    CartItemServices cartItemService;
    @MockBean
    private CartItemRepository cartItemRepository;

    @MockBean
    private ProfileRepository profileRepository;
    private UUID profileId;
    private UUID wrongProfileId;
    private UUID profileIdForDeleteExceptionCase;
    private UUID itemId;
    private List<UUID> itemIds;
    private Profile profileForDeleteExceptionCase;
    private Profile profile;

    @BeforeEach
    public void setup() {
        profileId = UUID.randomUUID();
        wrongProfileId = UUID.randomUUID();
        profileIdForDeleteExceptionCase = UUID.randomUUID();
        itemId = UUID.randomUUID();
        itemIds = new ArrayList<>();
        itemIds.add(itemId);
        profile = new Profile(profileId, "Bob");
        profileForDeleteExceptionCase = new Profile(profileIdForDeleteExceptionCase,"Bug");
        //for correct execution
        when(profileRepository.findById(profileId)).thenReturn(Optional.of(profile));
        when(cartItemRepository.existsByProfileAndItemId(profile, itemIds)).thenReturn(false);
        //for UserNotFoundException case
        when(profileRepository.findById(wrongProfileId)).thenReturn(Optional.empty());
        //for FailedToDeleteItemsException case
        when(profileRepository.findById(profileIdForDeleteExceptionCase)).thenReturn(Optional.of(profileForDeleteExceptionCase));
        when(cartItemRepository.existsByProfileAndItemId(profileForDeleteExceptionCase, itemIds)).thenReturn(true);
    }

    @Test
    void deleteItemsByIdAndItemIdsShouldBeExecutedCorrectly(){
        assertAll(()->cartItemService.deleteItemsByIdAndItemIds(profileId, itemIds));
    }

    @Test
    void deleteItemsByIdAndItemIdsShouldThrowUserNotFoundException(){
        assertThrows(UserNotFoundException.class,() -> cartItemService
                .deleteItemsByIdAndItemIds(wrongProfileId, itemIds),
                "UserNotFoundException expected to be thrown, but it didn't");
    }

    @Test
    void deleteItemsByIdAndItemIdsShouldThrowFailedToDeleteItemsException(){
        assertThrows(FailedToDeleteItemsException.class,() -> cartItemService
                        .deleteItemsByIdAndItemIds(profileIdForDeleteExceptionCase, itemIds),
                "FailedToDeleteItemsException expected to be thrown, but it didn't");
    }
}
