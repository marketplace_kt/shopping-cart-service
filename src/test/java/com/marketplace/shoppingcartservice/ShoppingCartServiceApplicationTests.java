
package com.marketplace.shoppingcartservice;

import com.marketplace.shoppingcartservice.dto.CartItemMessage;
import com.marketplace.shoppingcartservice.entity.CartItem;
import com.marketplace.shoppingcartservice.entity.Client;
import com.marketplace.shoppingcartservice.services.KafkaMessageService;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.kafka.test.context.EmbeddedKafka;
import org.springframework.test.annotation.DirtiesContext;
import org.testcontainers.shaded.com.fasterxml.jackson.core.JsonProcessingException;
import org.testcontainers.shaded.com.fasterxml.jackson.databind.ObjectMapper;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@SpringBootTest
@DirtiesContext
@EmbeddedKafka(partitions = 1, brokerProperties = {"listeners=PLAINTEXT://localhost:9092", "port=9092"})
@Slf4j
class ShoppingCartServiceApplicationTests {

	@Autowired
	private KafkaMessageService kafkaMessageService;

	public String testOrderFormation() {
		UUID itemId = UUID.randomUUID();
		UUID profileId = UUID.randomUUID();
		Client client = new Client(profileId, "User1");
		List<CartItem> cartItemList = new ArrayList<>();
		cartItemList.add(new CartItem(1111L, itemId, client, 1111, 1111L, true, 1111L));
		cartItemList.add(new CartItem(2222L, itemId, client, 2222, 2222L, true, 2222L));

		ObjectMapper objectMapper = new ObjectMapper();
		String message;
		try {
			message = objectMapper.writeValueAsString(new CartItemMessage(profileId, cartItemList));
			log.info("Имитация получения списка товаров из корзины и добавление товаров в заказ клиента с ID: {}", profileId);
		} catch (JsonProcessingException e) {
			log.info("Не удалось преобразовать список товаров в сообщение JSON формата");
			throw new RuntimeException(e);
		}
		return message;
	}

	@Test
	void testKafkaMessageService() {
		kafkaMessageService.sendingMessage("test-topic", testOrderFormation());
		log.info("Сообщение отправленно в топик заказов");
	}


}
