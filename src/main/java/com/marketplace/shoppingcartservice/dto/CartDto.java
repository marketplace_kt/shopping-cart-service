package com.marketplace.shoppingcartservice.dto;

import lombok.AllArgsConstructor;
import lombok.Data;


import java.util.List;
@Data
@AllArgsConstructor
public class CartDto {
    List<CartItemDto> cartItemDtoList;

    Long fullPrice;

    Long discPrice;

    Integer personalDiscountPercent;

}
