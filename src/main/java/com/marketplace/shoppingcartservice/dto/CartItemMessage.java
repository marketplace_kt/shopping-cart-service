package com.marketplace.shoppingcartservice.dto;

import com.marketplace.shoppingcartservice.entity.CartItem;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.context.annotation.Profile;

import java.util.List;
import java.util.UUID;


@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Profile("testKafka")
public class CartItemMessage {
    UUID uuid;
    List<CartItem> list;
}
