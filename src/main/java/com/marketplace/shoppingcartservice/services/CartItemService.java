package com.marketplace.shoppingcartservice.services;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.marketplace.shoppingcartservice.dto.CartDto;
import com.marketplace.shoppingcartservice.dto.CartItemDto;
import com.marketplace.shoppingcartservice.dto.CartItemMessage;
import com.marketplace.shoppingcartservice.entity.CartItem;
import com.marketplace.shoppingcartservice.entity.Client;
import com.marketplace.shoppingcartservice.exception.ClientNotFoundException;
import com.marketplace.shoppingcartservice.exception.FailedToDeleteItemsException;
import com.marketplace.shoppingcartservice.exception.JsonMessageCreationException;
import com.marketplace.shoppingcartservice.exception.UserNotFoundException;
import com.marketplace.shoppingcartservice.repositories.CartItemRepository;
import com.marketplace.shoppingcartservice.repositories.ClientRepository;
import com.marketplace.shoppingcartservice.repositories.PersonalDiscountRepository;
import com.marketplace.shoppingcartservice.util.CartItemMapper;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static com.marketplace.shoppingcartservice.config.KafkaConfig.TOPIC_FOR_ORDERS;

@Slf4j
@Service
@Transactional
@AllArgsConstructor
public class CartItemService {
    private final PersonalDiscountRepository personalDiscountRepository;

    private final CartItemRepository cartItemRepositories;
    private final ClientRepository clientRepository;
    private final CartItemMapper cartItemMapper;

    private final KafkaMessageService kafkaMessageService;

    public boolean existsByUUIDClient(UUID id) {
        return clientRepository.existsById(id);
    }

    public CartItem addCartItem(CartItemDto cartItemDto, UUID id) {
        log.info("Отправлен запрос на добавление товара {} в корзину клиента с id {}", cartItemDto, id);
        if (!existsByUUIDClient(id)) {
            log.info("Клиент с id {} для добавления товара {} в корзину не найден", id, cartItemDto);
            throw new ClientNotFoundException("клиент не найден");
        }
        CartItem cartItem = cartItemMapper.convertDtoToItem(cartItemDto);
        CartItem ci = cartItemRepositories.save(cartItem);
        log.info("Товар {} для клиента с id {} добавлен", cartItem, id);
        return ci;
    }

    /**
     * Получаем список товаров из корзины по
     * @param clientId - id клиента,
     * конвертируем все товары в ДТО
     *                 и получаем полную цену товаров и их цену со скидкой
     * @see CartItem - ДТО товара,
     * @return возвращаем ДТО корзины
     * с записаными в него полями
     * @see CartDto#cartItemDtoList
     * @see CartDto#fullPrice
     * @see CartDto#discPrice
     */
    public CartDto getCartDto(UUID clientId) {
        if(clientRepository.findById(clientId).isPresent()){
            log.info("Клиент с id {} найден,товары складываются в корзину",clientId);
        }
        List<CartItem> allByClientId = cartItemRepositories.findAllByClientId(clientId);
        List<CartItemDto> cartItemDtoList = new ArrayList<>();
        for (CartItem cartItem : allByClientId) {
            cartItemDtoList.add(cartItemMapper.convertItemToDto(cartItem));
        }
        long fullPrice = 0L;
        long discountPrice = 0L;
        Integer personalDiscountPercent = personalDiscountRepository.findByClientId(clientId).getDiscountPercent();
        log.info("Персональная скидка клиента {}%",personalDiscountPercent);
        for (CartItemDto cartItemDto : cartItemDtoList) {
            long discountPriceWithCount = cartItemDto.getCount() * cartItemDto.getPrice();
            fullPrice += discountPriceWithCount;
            if (cartItemDto.getIsDiscounted()) {
                discountPrice = discountPrice + cartItemDto.getCount() * cartItemDto.getDiscountPrice();
            }else {
                discountPrice+=discountPriceWithCount;
            }
        }
        log.info("Цена на товары без учета персональной скидки - {}",discountPrice);
        discountPrice = discountPrice - discountPrice / 100L * personalDiscountPercent;
        CartDto cartDto = new CartDto(cartItemDtoList, fullPrice, discountPrice,personalDiscountPercent);
        log.info("Корзина клиента с id {} готова",clientId);
        return cartDto;
    }

    public String orderFormationByClientId(UUID id) {
        log.info("Получение списка товаров клиента из корзины и добавление товаров в заказ клиента с ID: {}", id);
        ObjectMapper objectMapper = new ObjectMapper();
        String message;
        try {
            message = objectMapper.writeValueAsString(new CartItemMessage(id, cartItemRepositories.getCartItemListByClientId(id)));
            kafkaMessageService.sendingMessage(TOPIC_FOR_ORDERS, message);
            log.info("Сообщение отправлено в топик заказов");
        } catch (JsonProcessingException e) {
            log.error("Не удалось преобразовать список товаров в сообщение JSON формата");
            throw new JsonMessageCreationException(e);
        }
        return message;
    }

    /**
     * Метод удаляет из корзины товар по id пользователя и списку itemId товаров, которые нужно удалить.
     */
    public void deleteItemsByIdAndItemIds(UUID id, List<UUID> itemId) {
        Client client = clientRepository.findById(id).orElseThrow(() -> new UserNotFoundException(id));
        cartItemRepositories.deleteByClientAndItemId(client, itemId);
        if (cartItemRepositories.existsByClientAndItemId(client, itemId)) {
            throw new FailedToDeleteItemsException(id, itemId);
        }
    }

}


