package com.marketplace.shoppingcartservice.services;

import com.marketplace.shoppingcartservice.entity.Client;
import com.marketplace.shoppingcartservice.exception.ClientNotFoundException;
import com.marketplace.shoppingcartservice.repositories.ClientRepository;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


@Slf4j
@Service
@Transactional(readOnly = true)
@AllArgsConstructor
public class ClientService {
    private final ClientRepository clientRepository;

    public Client findClientByName(String name) {
        Client client = clientRepository.findByName(name)
                .orElseThrow(() -> new ClientNotFoundException(String.format("Клиент с именем %s не найден",name)));
        log.info("Клиент с именем {} найден",name);
        return client;
    }
}
