package com.marketplace.shoppingcartservice.services;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Profile;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
@Slf4j
@Profile("testKafka")
public class KafkaMessageService {
    private final KafkaTemplate<String, String> kafkaTemplate;

    public void sendingMessage(String topic, String message) {
        log.info("Содержимое сообщения в формате JSON: {}", message);
        kafkaTemplate.send(topic, message);
    }

}
