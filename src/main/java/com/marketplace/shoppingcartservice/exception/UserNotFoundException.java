package com.marketplace.shoppingcartservice.exception;

import lombok.extern.slf4j.Slf4j;

import java.util.UUID;

@Slf4j
public class UserNotFoundException extends RuntimeException{
    public UserNotFoundException(UUID profileId) {
        super("User with id " + profileId + " doesn't exist");
    }
}
