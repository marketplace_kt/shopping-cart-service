package com.marketplace.shoppingcartservice.exception;

public class JsonMessageCreationException extends RuntimeException {
    public JsonMessageCreationException(Throwable cause) {
        super(cause);
    }
}
