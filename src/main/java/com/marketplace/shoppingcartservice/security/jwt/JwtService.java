package com.marketplace.shoppingcartservice.security.jwt;


import com.marketplace.shoppingcartservice.entity.Client;
import com.marketplace.shoppingcartservice.services.ClientService;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.io.Decoders;
import io.jsonwebtoken.security.Keys;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.security.Key;
import java.util.Date;
import java.util.function.Function;

/**
 * Класс для работы с JWT
 */
@Service
@Slf4j
@RequiredArgsConstructor
public class JwtService {

    @Value("${environments.jwtSecretKey}")
    private String SECRET_KEY;
    private final ClientService clientService;

    /**
     * @return все требования
     */
    private Claims extractAllClaims(String token) {

        Claims claims = Jwts.parserBuilder()
                .setSigningKey(getSigningKey())
                .build()
                .parseClaimsJws(token.trim())
                .getBody();
        log.info("Claims(Требования) from JWT : {}", claims);
        return claims;
    }

    /**
     * @return ключ из его строкового представления
     */
    private Key getSigningKey() {
        byte[] keyBytes = Decoders.BASE64.decode(SECRET_KEY);
        return Keys.hmacShaKeyFor(keyBytes);

    }

    /**
     * @param token         - наш токен
     * @param claimResolver - функциональный интерфейс Function передаем лямбда выражение либо
     *                      ссылку на метод" (Method Reference) вида Claims::get...
     * @return требование(claim)
     */
    public <T> T extractClaim(String token, Function<Claims, T> claimResolver) {
        final Claims claims = extractAllClaims(token);
        return claimResolver.apply(claims);
    }

    /**
     * @return юзернейм пользователя
     */
    public String extractUsername(String token) {
        var username = extractClaim(token, Claims::getSubject);
        log.info("Username from JWT extracted : {}", username);
        return username;
    }

    /**
     * @return true Если токен прошел валидацию
     */

    public boolean isTokenValid(String token) {
        final String username = extractUsername(token);
        Client client = clientService.findClientByName(username);
        boolean isValid = username.equals(client.getName()) && !isTokenExpired(token);
        log.info("Token validation result : {}", isValid);
        return isValid;
    }

    /**
     * @return true Если токен не просрочен
     */
    private boolean isTokenExpired(String token) {
        Date tokenDate = extractClaim(token, Claims::getExpiration);
        log.info("JW Token created at {}", extractClaim(token, Claims::getIssuedAt));
        log.info("JW Token expiration date {}", tokenDate);
        return tokenDate.before(new Date());
    }
}
