package com.marketplace.shoppingcartservice.security.jwt;

import com.marketplace.shoppingcartservice.exception.JwtValidationException;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.lang.NonNull;


import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;


import javax.servlet.FilterChain;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Класс выполняющий фильтрацию каждого запроса,проверяющий корректность JWT
 */

@Slf4j
@Component
@RequiredArgsConstructor
public class JwtFilter extends OncePerRequestFilter {

    private final JwtService jwtService;


    /**
     * Фильтр принимает 3 не нулевых параметра,запрос,ответ,цепочка фильтров.
     * Извлекаем из запроса заголовок "Authorization" в котором находится JWT
     * Проверяем есть ли у нас вообще токен jwt в запросе и начинается ли он с "Bearer "
     * и если все норм то извлекаем jwtToken из заголовка обрезая начало(Bearer ) 7 символов
     * извлекаем из токена (username)логин пользователя
     */
    @Override
    protected void doFilterInternal(
            @NonNull HttpServletRequest request,
            @NonNull HttpServletResponse response,
            @NonNull FilterChain filterChain)
            throws IOException {
        log.info("JWT FILTER STARTED");

        final String authHeader = request.getHeader("Authorization");

        try {
            if (authHeader == null || !authHeader.startsWith("Bearer ")) {

                throw new JwtValidationException("JWT not found");

            }
        } catch (JwtValidationException e) {

            response.setStatus(401);
            response.getWriter().write("\"JWT Authorization error\" : Authorization failed (JWT is missing) " +
                    "\n \"status\" : " + response.getStatus());
            log.error("Authorization failed (JWT is missing)");
            return;

        }
        try {

            final String jwtToken = authHeader.substring(7);

            if (jwtService.isTokenValid(jwtToken)) {

                log.info("JWT прошел все проверки");
                filterChain.doFilter(request, response);

            } else throw new JwtValidationException("JWT validation error");

        } catch (Exception e) {

            response.setStatus(401);
            response.getWriter().write("\"JWT Authorization error\" : Authorization failed (JWT validation error) " +
                    "\n \"status\" : " + response.getStatus());
            log.error("JWT Authorization error");

        }
    }
}
