package com.marketplace.shoppingcartservice.repositories;

import com.marketplace.shoppingcartservice.entity.CartItem;
import com.marketplace.shoppingcartservice.entity.Client;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.UUID;

@Repository
public interface CartItemRepository extends CrudRepository<CartItem, Long> {
    List<CartItem> findAllByClientId(UUID clientId);

    @Transactional
    @Modifying
    @Query("delete from CartItem c where c.client = ?1 and c.itemId in ?2")
    void deleteByClientAndItemId(Client client, List<UUID> itemId);

    @Query("select (count(c) > 0) from CartItem c where c.client = ?1 and c.itemId in ?2")
    boolean existsByClientAndItemId(Client client, List<UUID> itemId);


    @Query("SELECT c from CartItem c where c.client.id= :id")
    List<CartItem> getCartItemListByClientId(UUID id);
}
