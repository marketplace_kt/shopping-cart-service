package com.marketplace.shoppingcartservice.repositories;

import com.marketplace.shoppingcartservice.entity.PersonalDiscount;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
public interface PersonalDiscountRepository extends JpaRepository<PersonalDiscount, Long> {
    PersonalDiscount findByClientId(UUID id);
}
