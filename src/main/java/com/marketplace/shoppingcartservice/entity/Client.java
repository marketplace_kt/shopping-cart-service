package com.marketplace.shoppingcartservice.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.UUID;

//сущность пользователя корзины

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "client", schema = "shopping_cart")
public class Client {
    @Id
    @Column(name = "id")
    private UUID id;    //    UUID пользователя

    @Column(name = "name")
    private String name;    //    имя пользователя
}
