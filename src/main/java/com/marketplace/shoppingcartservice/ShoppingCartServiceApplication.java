package com.marketplace.shoppingcartservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;

// TO-DO (exclude={SecurityAutoConfiguration.class}) - отключение базовой секьюрности!
// Убрать и настроить Spring Security совместно с JWT
@SpringBootApplication(exclude={SecurityAutoConfiguration.class})
@EnableEurekaClient
public class ShoppingCartServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(ShoppingCartServiceApplication.class, args);
	}

}
