package com.marketplace.shoppingcartservice.config;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.admin.NewTopic;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.kafka.config.TopicBuilder;


@Configuration
@RequiredArgsConstructor
@Slf4j
@Profile("testKafka")
public class KafkaConfig {

    public static final String TOPIC_FOR_ORDERS = "cart-service";

    @Bean
    public NewTopic topic() {
        return TopicBuilder
                .name(TOPIC_FOR_ORDERS)
                .partitions(1)
                .replicas(1)
                .build();
    }
}
