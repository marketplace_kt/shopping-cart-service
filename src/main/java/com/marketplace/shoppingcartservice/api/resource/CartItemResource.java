package com.marketplace.shoppingcartservice.api.resource;

import com.marketplace.shoppingcartservice.dto.CartDto;
import com.marketplace.shoppingcartservice.dto.CartItemDto;
import com.marketplace.shoppingcartservice.entity.CartItem;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import io.swagger.v3.oas.annotations.tags.Tags;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.UUID;

@Tags(
        value = {
                @Tag(name = "Корзина", description = "Товары из корзины")
        }
)
@RequestMapping("/cart-item")
public interface CartItemResource {
    @GetMapping("/{id}")
    @Operation(summary = "Вывод всех товаров в корзине")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Корзина отображается",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = List.class))}),
            @ApiResponse(responseCode = "400", description = "Неверный запрос",
                    content = @Content),
            @ApiResponse(responseCode = "404", description = "Корзина/пользователь не найден(а)",
                    content = @Content)})
        ResponseEntity<CartDto> showAllCartItems(@PathVariable @NotNull UUID id);

    @PostMapping("{id}")
    @Operation(summary = "Добавить товар в корзину")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Товар добавлен",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = CartItem.class)) }),
            @ApiResponse(responseCode = "400", description = "неверный запрос",
                    content = @Content),
            @ApiResponse(responseCode = "404", description = "товар не добавлен",
                    content = @Content) })
        ResponseEntity<CartItem> addCartItem(@RequestBody CartItemDto cartItemDto, @PathVariable UUID id);

    @DeleteMapping("/{id}/delete")
    @Operation(summary = "Удаление по идентификатору клиента и товара")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Товар удален",
                    content = @Content),
            @ApiResponse(responseCode = "500", description = "Возникла ошибка при удалении товара",
                    content = @Content)})
        ResponseEntity<HttpStatus> deleteCartItems(@PathVariable UUID id, @RequestParam List<UUID> itemId);

}
